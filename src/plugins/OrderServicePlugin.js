import OrderService from "../services/OrderService";

export default {
    install(Vue, options) {
        Vue.prototype.$orderService = new OrderService(options.baseUrl);
    }
}