import ProductService from "../services/ProductService";

export default {
    install(Vue, settings) {
        Vue.prototype.$productService = new ProductService(settings);
    }
}