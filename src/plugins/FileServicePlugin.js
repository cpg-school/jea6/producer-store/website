import FileService from "../services/fileService";

export default {
    install(Vue, config) {
        Vue.prototype.$fileService = new FileService();
    }
}