import AuthenticationService from "../services/AuthenticationService";

export default {
    install(Vue, options) {
        Vue.prototype.$authService = new AuthenticationService(options);
    }
}