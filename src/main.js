import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import GAuth from 'vue-google-oauth2'
import AuthenticationServicePlugin from './plugins/AuthenticationServicePlugin';
import ProductServicePlugin from './plugins/ProductServicePlugin';
import FileServicePlugin from "./plugins/FileServicePlugin";
import OrderServicePlugin from './plugins/OrderServicePlugin';

Vue.config.productionTip = false

Vue.use(AuthenticationServicePlugin, { baseUrl: "http://localhost:8004" })
Vue.use(ProductServicePlugin, { baseUrl: "http://localhost:8000" });
Vue.use(OrderServicePlugin, { baseUrl: "http://localhost:8003" })
Vue.use(FileServicePlugin);

Vue.use(GAuth, {
  clientId: '550217152689-10cdo5dv9hmrm1ckcgo33053sf45njil.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
