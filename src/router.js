import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Product from './views/Product.vue'
import Admin from './views/Admin.vue'
import Shoppingcart from './views/Shoppingcart.vue'
import Orders from './views/Orders.vue'

import store from "./store"
import { decodeToken } from "./services/AuthenticationService";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: "/product/:id",
      name: 'product',
      component: Product
    },
    {
      path: "/winkelwagen",
      name: "shoppingcart",
      component: Shoppingcart
    },
    {
      path: "/bestellingen",
      name: "orders",
      component: Orders
    },
    {
      path: "/admin",
      name: "admin",
      canReuse: false,
      component: Admin,
      beforeEnter: (to, from, next) => {
        if (!store.state.user || !store.state.user.token) {
          next({ name: 'home', query: { reason: 'unauthorized', message: "Je moet ingelogd zijn om deze pagina te bekijken." } })
        }

        const user = decodeToken(store.state.user.token);

        if (!user.roles.includes('admin')) {
          next({ name: 'home', query: { reason: 'unauthorized', message: "Je moet admin rechten hebben om deze pagina te bekijken." } })
        } else {
          next();
        }
      }
    }
  ]
})
