export const randomName = () => {
    const names = [
        "Henk",
        "Freek",
        "Frits",
        "Janus",
        "Koeplam"
    ]

    return names[Math.floor(Math.random() * names.length)]
}

export const orThrow = (message) => { throw Error(message) }