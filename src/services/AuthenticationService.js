import decodeJwt from "jwt-decode"

export default class AuthenticationService {

    constructor(config) {
        this.baseUrl = config.baseUrl || "http://localhost:8080/producerstore/api";
    }

    async authenticate(username, password) {
        if (!username || !password) {
            throw new Error("Unauthorized, no password or username provided")
        }

        const reponse = await fetch(`${this.baseUrl}/authentication`, {
            method: "POST",
            body: JSON.stringify({ username, password }),
            headers: {
                "Content-Type": "application/json"
            }
        });

        if (reponse.status != 200) {
            throw new Error("Unauthorized, wrong response.")
        }

        const token = await reponse.json();

        return decodeToken(token.token);
    }

    async oAuth(accessToken) {
        if (!accessToken) {
            throw new Error("Unauthorized, no password or username provided")
        }

        const reponse = await fetch(`${this.baseUrl}/authentication/oath/google`, {
            method: "POST",
            body: accessToken,
            headers: {
                "Content-Type": "text/plain"
            }
        });

        const token = await reponse.json();

        return decodeToken(token.token, 'google');
    }
}

export const decodeToken = (token, oath = false) => {
    const decoded = decodeJwt(token);
  
    return {
        id: decoded.id,
        name: decoded.sub,
        roles: decoded.roles,
        oauth: oath,
        token: token,
    }
}