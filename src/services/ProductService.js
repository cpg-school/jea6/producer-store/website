export default class ProductService {
    
    constructor(config) {
        this.baseUrl = config.baseUrl || "http://localhost:8080/producerstore/api";
    }
    
    async getAll() {
        const response = await fetch(`${this.baseUrl}/products`)
        return await response.json();
    }

    async getById(id) {
        if (!id) {
            return undefined;
        }

        const response = await fetch(`${this.baseUrl}/products/${id}`)

        return await response.json();
    }
}