import { orThrow } from "../utillities";
import store from "../store";

export default class OrderService {
    constructor(baseUrl) {
        this.baseUrl = baseUrl || orThrow("Please provide a baseUrl for OrderService");
    }

    async getOrdersForCurrentUser() {
        const token = store.state.user.token;

        if (!token) {
            throw "No user logged in."
        }

        const response = await fetch(this.baseUrl + "/users/me/orders", { headers: { "Authorization": `Bearer ${token}` } })

        if (response.status == 204) {
            throw "No orders for user."
        }

        return await response.json();
    }

    async createOrder(products) {
        const token = store.state.user.token;

        if (!token) {
            throw "Je moet ingelogd zijn om af te rekenen."
        }

        const orderLines = this.toOrderLines(products);

        await fetch(this.baseUrl + "/users/me/orders", {
            headers: {
                "Authorization": `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(orderLines),
            method: "POST"
        });
    }

    toOrderLines(products) {
        return products.map(p => ({ amount: p.amount, product: { id: p.id, name: p.name, price: p.price } }))
    }
}