import Table from "easy-table";

export default class FileService {
    download(username, productLines) {
        const table = new Table();

        for (let productLine of productLines) {
            table.cell('Product', productLine.product.name);
            table.cell('Aantal', productLine.amount);
            table.cell('Prijs', `€${(productLine.amount * productLine.product.price).toFixed(2)}`);
            table.newRow();
        }

        table.total('Prijs', {
            reduce: (acc, val, i) => acc += parseFloat(val.replace('€', '')),
            printer: (val) => `€${val.toFixed(2)}`
        })

        let text = `Bedankt voor uw aankoop bij Producerstore.nl, ${username}!\n\n`;
        text += table.toString();

        this._download("Uw aankopen bij Producerstore.txt", text);
    }

    _download(filename, text) {
        const element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }
}