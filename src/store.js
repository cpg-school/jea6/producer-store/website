import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      id: undefined,
      name: undefined,
      token: undefined,
      isGoogle: undefined
    },
    shoppingCart: {
      products: []
    }
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },

    resetUser(state, user) {
      state.user.id = undefined;
      state.user.name = undefined;
      state.user.token = undefined;
      state.user.isGoogle = undefined;
    },

    addToCart(state, product) {
      if (!state.shoppingCart.products.some(p => p.id == product.id)) {
        product.amount = 1;
        state.shoppingCart.products.push(product);
      } else {
        console.log("Product already in cart.");
        state.shoppingCart.products.find(p => p.id == product.id).amount++;
      }
    },

    removeFromCart(state, productId) {
      state.shoppingCart.products = state.shoppingCart.products.filter(p => p.id !== productId);
    },

    clearCart(state) {
      state.shoppingCart.products = [];
    }
  },
  actions: {

  }
})
